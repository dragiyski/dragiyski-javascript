(function () {
    const JavaScript = {
        REGEXP_UNICODE_SEQUENCES: /\\u([0-9a-fA-F]{4}|{([0-9a-fA-F]+)})/g,
        REGEXP_IDENTIFIER: require('./regexp.identifier'),
        deserializeUnicodeSequences(string) {
            return string.replace(this.REGEXP_UNICODE_SEQUENCES, (match, s1, s2) => {
                const code = s2 != null ? parseInt(s2, 16) : parseInt(s1, 16);
                return String.fromCodePoint(code);
            });
        },
        isIdentifier(string) {
            return this.REGEXP_IDENTIFIER.test(this.deserializeUnicodeSequences(string));
        },
        canStoreProperties(value) {
            if (value == null) {
                return false;
            }
            const type = typeof value;
            return type === 'object' || type === 'function';
        },
        // istanbul ignore next
        getType(value) {
            if (value === void 0) {
                return 'undefined';
            } else if (value === null) {
                return 'null';
            } else {
                const type = typeof value;
                if (type === 'function') {
                    let name = value.name;
                    if (typeof name === 'string') {
                        name = name.trim();
                        if (name.length > 0) {
                            return `[${type} ${name}]`;
                        }
                    }
                    return `${type}`;
                } else if (type === 'object') {
                    return Object.prototype.toString.call(value);
                } else {
                    return `${type}`;
                }
            }
        },
        createPrimordials(primordials, global) {
            const copyAll = (prefix, source) => {
                for (const name of global.Object.getOwnPropertyNames(source)) {
                    const descriptor = global.Object.getOwnPropertyDescriptor(source, name);
                    if (this.canStoreProperties(descriptor.value)) {
                        if (recursionDetection.has(descriptor.value)) {
                            continue;
                        }
                        recursionDetection.add(descriptor.value);
                    }
                    copyProperty(`${prefix}${name}`, descriptor);
                    if (global.Object.hasOwnProperty.call(descriptor, 'value')) {
                        if (this.canStoreProperties(descriptor.value)) {
                            copyAll(`${prefix}${name}.`, descriptor.value);
                        }
                    }
                    if (this.canStoreProperties(descriptor.value)) {
                        recursionDetection.delete(descriptor.value);
                    }
                }
            };

            const copyProperty = (primordialName, descriptor) => {
                if (global.Object.hasOwnProperty.call(descriptor, 'value')) {
                    Object.defineProperty(primordials, primordialName, {
                        enumerable: true,
                        value: descriptor.value
                    });
                } else /* istanbul ignore else */ if (global.Object.hasOwnProperty.call(descriptor, 'get')) {
                    Object.defineProperty(primordials, `${primordialName}[[get]]`, {
                        enumerable: true,
                        value: descriptor.get
                    });
                    if (global.Object.hasOwnProperty.call(descriptor, 'set') && descriptor.set != null) {
                        Object.defineProperty(primordials, `${primordialName}[[set]]`, {
                            enumerable: true,
                            value: descriptor.set
                        });
                    }
                } else {
                    // This should be unreachable in any proper ECMAScript implementation (for the current standard).
                    /* istanbul ignore next */
                    throw new Error('Expected ECMAScript descriptor to be either data property (with [value)) or accessor property (with [get] and optional [set])');
                }
            };
            const recursionDetection = new Set();
            recursionDetection.add(global);
            copyAll('', global);
        },
        prototypeof(object, prototype) {
            if (object == null || typeof object !== 'object') {
                return false;
            }
            object = Object.getPrototypeOf(object);
            if (prototype == null) {
                return object == null;
            }
            while (object != null) {
                if (object === prototype) {
                    return true;
                }
                object = Object.getPrototypeOf(object);
            }
            return false;
        },
        instanceof(object, Class) {
            if (typeof Class !== 'function') {
                throw new TypeError(`Right-hand side of 'instanceof' is not callable`);
            }
            if (Class.prototype == null) {
                throw new TypeError(`Function has non-object prototype 'null' in instanceof check`);
            }
            return this.prototypeof(object, Class.prototype);
        },
        flattenCallSite(callSite) {
            const result = {};
            const names = Object.getOwnPropertyNames(Object.getPrototypeOf(callSite));
            for (const name of names) {
                if (typeof callSite[name] === 'function') {
                    if (name.startsWith('get')) {
                        let resultName = name.substr(3);
                        resultName = resultName.substr(0, 1).toLowerCase() + resultName.substr(1);
                        result[resultName] = callSite[name]();
                    } else if (name.startsWith('is')) {
                        let resultName = name.substr(2);
                        resultName = resultName.substr(0, 1).toLowerCase() + resultName.substr(1);
                        result[resultName] = callSite[name]();
                    }
                }
            }
            return result;
        },
        stackTrace(error) {
            const prepareStackTraceDescriptor = Object.getOwnPropertyDescriptor(Error, 'prepareStackTrace');
            if (prepareStackTraceDescriptor != null && !prepareStackTraceDescriptor.configurable) {
                return null;
            }
            let trace = null;
            Object.defineProperty(Error, 'prepareStackTrace', {
                configurable: true,
                writable: true,
                // eslint-disable-next-line handle-callback-err
                value: function (error, callSites) {
                    if (Array.isArray(callSites)) {
                        trace = callSites.map(JavaScript.flattenCallSite);
                    }
                }
            });
            const stack = error.stack;
            if (typeof stack === 'string') {
                return stack;
            }
            if (prepareStackTraceDescriptor == null) {
                delete Error.prepareStackTrace;
            } else {
                Object.defineProperty(Error, 'prepareStackTrace', prepareStackTraceDescriptor);
            }
            return trace;
        },
        mixin(constructor, prototype, { name = null, root = prototype } = {}) {
            if (prototype == null || !JavaScript.canStoreProperties(prototype)) {
                throw new TypeError('Expected an object for a prototype');
            }
            constructor.prototype = Object.create(prototype, {
                constructor: {
                    value: constructor
                }
            });
            const prototypeSet = new WeakSet();
            let exclude = null;
            if (root != null) {
                exclude = Object.getPrototypeOf(root);
            }
            Object.defineProperties(constructor, {
                extends: {
                    configurable: true,
                    writable: true,
                    value: function (target) {
                        if (target == null || !JavaScript.canStoreProperties(target)) {
                            throw new TypeError('Expected the target argument to be an object');
                        }
                        if (!prototypeSet.has(target)) {
                            prototypeSet.add(target);
                            let object = prototype;
                            while (object != null && object !== exclude) {
                                for (const name of Object.getOwnPropertyNames(object)) {
                                    if (name === 'constructor') {
                                        continue;
                                    }
                                    if (Object.hasOwnProperty.call(target, name)) {
                                        continue;
                                    }
                                    const descriptor = Object.getOwnPropertyDescriptor(object, name);
                                    Object.defineProperty(target, name, descriptor);
                                }
                                for (const symbol of Object.getOwnPropertySymbols(object)) {
                                    if (Object.hasOwnProperty.call(target, symbol)) {
                                        continue;
                                    }
                                    const descriptor = Object.getOwnPropertyDescriptor(object, symbol);
                                    Object.defineProperty(target, symbol, descriptor);
                                }
                                object = Object.getPrototypeOf(object);
                            }
                        }
                    }
                },
                [Symbol.hasInstance]: {
                    configurable: true,
                    writable: true,
                    value: function (instance) {
                        if (instance == null) {
                            return false;
                        }
                        let object = Object.getPrototypeOf(instance);
                        while (object != null) {
                            if (object === constructor.prototype || object === prototype || prototypeSet.has(object)) {
                                return true;
                            }
                            object = Object.getPrototypeOf(object);
                        }
                        return false;
                    }
                }
            });
            return constructor;
        }
    };

    JavaScript.primordialsGlobalHandler = {
        traceGlobal(path) {
            let object = globalThis;
            while (path.length > 1) {
                const key = path.shift();
                const descriptor = Object.getOwnPropertyDescriptor(object, key);
                if (descriptor == null || descriptor.value == null) {
                    return null;
                }
                object = descriptor.value;
            }
            return object;
        },
        getDescription(object, key) {
            const description = {
                object,
                key,
                descriptorKey: 'value'
            };
            if (key.endsWith('[[get]]')) {
                description.descriptorKey = 'get';
                description.key = key.substr(0, key.length - 7);
            } else if (key.endsWith('[[set]]')) {
                description.descriptorKey = 'set';
                description.key = key.substr(0, key.length - 7);
            }
            description.descriptor = Object.getOwnPropertyDescriptor(object, description.key);
            return description;
        },
        cacheDescriptor(target, property) {
            let descriptor = Object.getOwnPropertyDescriptor(target, property);
            if (descriptor == null && typeof property === 'string') {
                const path = property.split('.');
                const object = this.traceGlobal(path);
                if (object != null) {
                    const description = this.getDescription(object, path.shift());
                    if (description.descriptor != null && Object.hasOwnProperty.call(description.descriptor, description.descriptorKey)) {
                        descriptor = {
                            configurable: false,
                            enumerable: true,
                            writable: false,
                            value: description.descriptor[description.descriptorKey]
                        };
                        Object.defineProperty(target, property, descriptor);
                    }
                }
            }
            return descriptor;
        },
        getOwnPropertyDescriptor(target, property) {
            const descriptor = this.cacheDescriptor(target, property);
            if (descriptor != null) {
                return descriptor;
            }
            return Reflect.getOwnPropertyDescriptor(target, property);
        },
        has: function (target, property) {
            const descriptor = this.cacheDescriptor(target, property);
            if (descriptor != null) {
                return true;
            }
            return Reflect.has(target, property);
        },
        get: function (target, property, receiver) {
            const descriptor = this.cacheDescriptor(target, property);
            if (descriptor != null && Object.hasOwnProperty.call(descriptor, 'value')) {
                return descriptor.value;
            }
            return Reflect.get(target, property, receiver);
        }
    };

    module.exports = JavaScript;
})();
